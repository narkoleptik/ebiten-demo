package main

import (
	"embed"
	"errors"
	"math/rand"
	"os"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/audio"
	"github.com/hajimehoshi/ebiten/v2/inpututil"

	"github.com/tinne26/etxt"
)

const (
	screenWidth  = 1024
	screenHeight = 768
	gameTitle    = "Demo"
	sampleRate   = 44100
)

var (
	//go:embed resources
	resourceRootFS embed.FS

	//go:embed resources/fonts/spaceage.otf
	spaceage_otf []byte

	//go:embed resources/fonts/kelly.ttf
	kelly_ttf []byte

	//go:embed resources/sounds/fire.wav
	fire_wav []byte

	//go:embed resources/sounds/powerup.wav
	powerup_wav []byte

	//go:embed resources/images/gopher.png
	gopher_png []byte

	audioContext *audio.Context
	volume       = 8.0

	s1 = rand.NewSource(time.Now().UnixNano())
	r1 = rand.New(s1)

	// Game options
	loading = true
)

func init() {
	audioContext = audio.NewContext(sampleRate)
}

func (g *Game) init() {
	g.currentMode = 0
	g.previousMode = 0
	g.currentScreen = ebiten.NewImage(screenWidth, screenHeight)
}

// Game structure
type Game struct {
	txtRenderer   *etxt.Renderer
	redSrc        float64
	greenSrc      float64
	blueSrc       float64
	count         int
	currentMode   int
	previousMode  int
	currentScreen *ebiten.Image
}

func (g *Game) updateMode(i int) {
	g.redSrc -= 0.0202
	g.greenSrc -= 0.0168
	g.blueSrc -= 0.0227

	g.previousMode = g.currentMode
	g.currentMode = i
}

// Update proceeds the game state.
// Update is called every tick (1/60 [s] by default).
func (g *Game) Update() error {
	g.count++

	switch g.currentMode {
	case 1:
		if inpututil.IsKeyJustPressed(ebiten.KeyP) {
			PlaySE(fire_wav) //, sampleRate)
		}
		if inpututil.IsKeyJustPressed(ebiten.KeyH) {
			PlaySEwithVolume(fire_wav, (volume / 2))
		}
		if inpututil.IsKeyJustPressed(ebiten.KeyL) {
			volume--
		}
		if inpututil.IsKeyJustPressed(ebiten.KeyR) {
			volume++
		}
		if ebiten.IsKeyPressed(ebiten.KeyLeft) {
			gopherAngle--
			if gopherAngle < 0 {
				gopherAngle = 360
			}
		}
		if ebiten.IsKeyPressed(ebiten.KeyRight) {
			gopherAngle++
			if gopherAngle > 360 {
				gopherAngle = 0
			}
		}
		if inpututil.IsKeyJustPressed(ebiten.KeyQ) {
			return errors.New("game shutdown")
		}
	default:
	}

	return nil
}

// Draw draws the game screen.
// Draw is called every frame (typically 1/60[s] for 60Hz display).
func (g *Game) Draw(screen *ebiten.Image) {
	// Write your game's rendering.
	var vscreen *ebiten.Image
	switch g.currentMode {
	case 0:
		vscreen = g.loadScreen()
	case 1:
		vscreen = g.menuScreen()
	default:
	}

	op := &ebiten.DrawImageOptions{}
	screen.DrawImage(vscreen, op)
}

// Layout defines the game screen
func (g *Game) Layout(_, _ int) (w, h int) {
	return screenWidth, screenHeight
}

func main() {
	// create cache
	cache := etxt.NewDefaultCache(1024 * 1024 * 1024) // 1GB cache

	// create and configure renderer
	renderer := etxt.NewStdRenderer()
	renderer.SetCacheHandler(cache.NewHandler())

	game := &Game{txtRenderer: renderer, redSrc: -5.54, greenSrc: -4.3, blueSrc: -6.4}

	ebiten.SetWindowSize(screenWidth, screenHeight)
	ebiten.SetWindowTitle(gameTitle)

	// Call ebiten.RunGame to start your game loop.
	if err := ebiten.RunGame(game); err != nil {
		if err.Error() == "game shutdown" {
			os.Exit(0)
		} else {
			check(err)
		}
	}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
