package main

import (
	"golang.org/x/image/font"
	"golang.org/x/image/font/opentype"
	"golang.org/x/image/font/sfnt"
)

var (
	myFont *sfnt.Font
)

// Font structure for defined fonts
type Font struct {
	Name string
	Size int
	Face font.Face
}

func (f *Font) size(s float64, fnt []byte) {
	parsedFont, err := opentype.Parse(fnt)
	check(err)

	face, err := opentype.NewFace(parsedFont, &opentype.FaceOptions{
		Size: s,
		DPI:  72,
	})
	check(err)

	f.Face = face
}

func init() {
	/*
		f, err := fs.Sub(FontRootFS, "fonts")
		check(err)

		fontFS = f

		fontFile, err := fontFS.Open("/spaceage.otf")
		check(err)
		defer fontFile.Close()
	*/

}
