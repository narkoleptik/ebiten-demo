package main

import (
	"fmt"
	"image/color"
	"log"
	"time"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/tinne26/etxt"
	"golang.org/x/image/colornames"
	"golang.org/x/image/font/sfnt"
)

var (
	firstrun      = true
	doneLoading   = false
	loadingScreen = ebiten.NewImage(screenWidth, screenHeight)
	fnt           *sfnt.Font
)

func init() {
	f, fontName, err := etxt.ParseFontBytes(kelly_ttf)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Font loaded: %s\n", fontName)
	fnt = f
}

func (g *Game) loadScreen() *ebiten.Image {
	loadingScreen.Clear()
	if firstrun {
		//PlaySEwithVolume(dialup_wav, 1.0)
		//go dialup()
		doneLoading = true
		firstrun = false
	}

	// Fill screen with solid color
	loadingScreen.Fill(colornames.Black)

	g.txtRenderer.SetTarget(loadingScreen)
	g.txtRenderer.SetFont(fnt)

	// Title
	titleSize := float64(24.0 + float64(g.count/4))
	if titleSize > 86.0 {
		titleSize = 86.0
	}
	title := "Ebiten Demo!"
	g.txtRenderer.SetSizePx(int(titleSize))
	g.txtRenderer.SetAlign(etxt.YCenter, etxt.XCenter)
	var clr color.RGBA
	switch g.count % 6 {
	case 0:
		clr = colornames.Red
	case 1:
		clr = colornames.Orange
	case 2:
		clr = colornames.Yellow
	case 3:
		clr = colornames.Lime
	case 4:
		clr = colornames.Blue
	case 5:
		clr = colornames.Purple
	default:
		clr = colornames.Red
	}
	g.txtRenderer.SetColor(clr)
	g.txtRenderer.Draw(title, screenWidth/2, screenHeight/2-int(g.txtRenderer.GetLineAdvance().Ceil()))

	// by line
	bySize := 18.0
	by := "by"
	g.txtRenderer.SetSizePx(int(bySize))
	g.txtRenderer.SetColor(colornames.Lime)
	g.txtRenderer.Draw(by, screenWidth/2, screenHeight/2)

	// author line
	authorSize := float64(24.0 + float64(g.count/8))
	if authorSize > 48.0 {
		authorSize = 48.0
	}
	author := "Amandreth"
	g.txtRenderer.SetSizePx(int(authorSize))
	g.txtRenderer.SetColor(colornames.Lime)
	g.txtRenderer.Draw(author, screenWidth/2, screenHeight/2+int(g.txtRenderer.GetLineAdvance().Ceil()))

	// copyright
	crSize := 16.0
	cr := "©2022"
	g.txtRenderer.SetSizePx(int(crSize))
	g.txtRenderer.SetColor(colornames.Lime)
	g.txtRenderer.Draw(cr, screenWidth/2, screenHeight-int(g.txtRenderer.GetLineAdvance().Ceil()))

	if titleSize == 86.0 && doneLoading {
		go g.closeLoad()
	}

	return loadingScreen
}

func (g *Game) closeLoad() {
	time.Sleep(2000 * time.Millisecond)
	g.updateMode(1)
}
