package main

import (
	"bytes"
	"fmt"
	"image"
	_ "image/png"
	"log"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/tinne26/etxt"
	"golang.org/x/image/colornames"
	"golang.org/x/image/font/sfnt"
)

var (
	menuScreen  = ebiten.NewImage(screenWidth, screenHeight)
	gopher      *ebiten.Image
	infoBox     *ebiten.Image
	gopherAngle = 0
	menuFont    *sfnt.Font
)

func init() {
	gopherDecoded, _, err := image.Decode(bytes.NewReader(gopher_png))
	check(err)
	gopher = ebiten.NewImageFromImage(gopherDecoded)

	f, fontName, err := etxt.ParseFontBytes(kelly_ttf)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Font loaded: %s\n", fontName)
	menuFont = f

	infoBox = ebiten.NewImage(screenWidth-20, 80)
	//ebitenutil.DrawRect(menuScreen, boxX, boxY, boxWidth, boxHeight, colornames.Black)
}

func (g *Game) menuScreen() *ebiten.Image {
	menuScreen.Clear()
	menuScreen.Fill(colornames.Gray)

	g.txtRenderer.SetTarget(menuScreen)
	g.txtRenderer.SetFont(menuFont)

	// Gopher image
	gopherOp := &ebiten.DrawImageOptions{}
	gw, gh := gopher.Size()
	gx, gy := float64(screenWidth-(screenWidth/8)), float64(screenHeight-(screenHeight/4)-gh+2)
	// move the image to be centered on 0,0
	gopherOp.GeoM.Translate(-float64(gw)/2, -float64(gh)/2)
	// rotate the image
	gopherOp.GeoM.Rotate(float64(gopherAngle%360) * 2 * math.Pi / 360)
	// move center of image to desired screen location
	gopherOp.GeoM.Translate(gx, gy)
	menuScreen.DrawImage(gopher, gopherOp)

	// Menu Text
	menuSize := 56.0
	shadow := 2
	menuText := fmt.Sprintf("P)lay SoundEffect at Current Volume\n")
	menuText = fmt.Sprintf("%sH)alf Volume SoundEffect\n", menuText)
	menuText = fmt.Sprintf("%sL)ower Volume\n", menuText)
	menuText = fmt.Sprintf("%sR)aise Volume\n", menuText)
	menuText = fmt.Sprintf("%sLeft and Right Arrows turn gopher\n", menuText)
	menuText = fmt.Sprintf("%sQ)uit", menuText)
	g.txtRenderer.SetAlign(etxt.YCenter, etxt.Left)
	g.txtRenderer.SetSizePx(int(menuSize))
	g.txtRenderer.SetColor(colornames.Black)
	menuTextWidth := int(g.txtRenderer.GetSizePxFract().Ceil())
	menuTextY := screenHeight / 2
	g.txtRenderer.Draw(menuText, menuTextWidth+shadow, menuTextY+shadow)
	g.txtRenderer.SetColor(colornames.Lime)
	g.txtRenderer.Draw(menuText, menuTextWidth, menuTextY)

	// Info Box at the bottom
	infoBox.Fill(colornames.Black)
	boxX := 10.0
	boxY := screenHeight - 100.0
	infoSize := 32.0
	infoText := fmt.Sprintf("Volume: %.0f Gopher Location: %.0f x %.0f(red dot) Angle: %d", volume, gx, gy, gopherAngle)
	g.txtRenderer.SetSizePx(int(infoSize))
	g.txtRenderer.SetColor(colornames.White)
	g.txtRenderer.SetTarget(infoBox)
	g.txtRenderer.SetAlign(etxt.YCenter, etxt.Left)
	infoBoxOp := &ebiten.DrawImageOptions{}
	infoBoxOp.GeoM.Translate(boxX, boxY)
	_, bh := infoBox.Size()
	g.txtRenderer.Draw(infoText, 10, bh/2)
	menuScreen.DrawImage(infoBox, infoBoxOp)

	// Place red dot in the center of the gopher image
	for i := 0; i < 2; i++ {
		for y := 0; y < 2; y++ {
			menuScreen.Set(int(gx)+i, int(gy)+y, colornames.Red)
		}
	}

	return menuScreen
}
