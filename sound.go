package main

import (
	"bytes"
	"io/ioutil"
	"log"

	"github.com/hajimehoshi/ebiten/v2/audio"
	"github.com/hajimehoshi/ebiten/v2/audio/wav"
)

// PlaySE is the sound effect player
func PlaySE(bs []byte) {
	PlaySEwithVolume(bs, volume)
}

// PlaySEwithVolume is the sound effect player
func PlaySEwithVolume(bs []byte, vol float64) {
	s, err := wav.DecodeWithSampleRate(sampleRate, bytes.NewReader(bs))
	if err != nil {
		log.Fatal(err)
		return
	}
	b, err := ioutil.ReadAll(s)
	if err != nil {
		log.Fatal(err)
		return
	}
	sePlayer := audio.NewPlayerFromBytes(audioContext, b)
	// sePlayer is never GCed as long as it plays.
	sePlayer.SetVolume(vol)
	sePlayer.Play()
}
